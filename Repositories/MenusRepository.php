<?php

namespace Gdev\MenuManagement\Repositories;


use Data\Repositories\BaseRepository;

class MenusRepository extends BaseRepository {

    const Model = 'Gdev\MenuManagement\Models\Menu';

}