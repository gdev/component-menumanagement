<?php

namespace Gdev\MenuManagement\Repositories;


use Data\Repositories\BaseRepository;

class MenuItemsRepository extends BaseRepository {

    const Model = 'Gdev\MenuManagement\Models\MenuItem';

}