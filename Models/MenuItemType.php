<?php

namespace Gdev\MenuManagement\Models;

use Spot\Entity;

/**
 * Class MenuItemType
 * @package Gdev\MenuManagement\Models
 *
 * @property integer MenuItemTypeId
 * @property string Caption
 */
class MenuItemType extends Entity {

    // Database Mapping
    protected static $table = "menu_item_types";

    public static function fields() {
        return [
            "MenuItemTypeId" => ['type' => 'integer', 'primary' => true],
            "Caption" => ['type' => 'string', 'required' => true]
        ];
    }


}