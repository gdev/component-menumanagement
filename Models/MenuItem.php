<?php

namespace Gdev\MenuManagement\Models;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Class MenuItem
 * @package Gdev\MenuManagement\Models
 *
 * @property integer MenuItemId
 * @property string Caption
 * @property integer ParentId
 * @property integer MenuItemTypeId
 * @property integer ObjectId
 * @property integer ObjectTypeId
 * @property integer MenuId
 * @property integer Weight
 * @property boolean Bold
 * @property Menu Menu
 * @property MenuItem Parent
 * @property MenuItemType MenuItemType
 * @property ObjectType ObjectType
 */
class MenuItem extends Entity {

    // Database Mapping
    protected static $table = "menu_items";

    public static function fields() {
        return [
            "MenuItemId" => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            "Caption" => ['type' => 'string', 'required' => true],
            "ParentId" => ['type' => 'integer'],
            "MenuItemTypeId" => ['type' => 'integer', 'required' => true],
            "ObjectId" => ['type' => 'integer'],
            "ObjectTypeId" => ['type' => 'integer'],
            "Bold" => ['type' => 'boolean'],
            "MenuId" => ['type' => 'integer', 'required' => true],
            "Weight" => ['type' => 'integer'],
        ];
    }


    public static function relations(MapperInterface $mapper, EntityInterface $entity) {
        return [
            'Menu' => $mapper->belongsTo($entity, 'Gdev\MenuManagement\Models\Menu', 'MenuId'),
            'Parent' => $mapper->belongsTo($entity, 'Gdev\MenuManagement\Models\MenuItem', 'ParentId'),
            'MenuItemType' => $mapper->belongsTo($entity, 'Gdev\MenuManagement\Models\MenuItemType', 'MenuItemTypeId'),
            'ObjectType' => $mapper->belongsTo($entity, 'Gdev\MenuManagement\Models\ObjectType', 'ObjectTypeId'),
        ];
    }


}