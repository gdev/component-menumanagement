<?php

namespace Gdev\MenuManagement\Models;

use Spot\Entity;

/**
 * Class MenuPosition
 * @package Gdev\MenuManagement\Models
 * 
 * @property integer MenuPositionId
 * @property string Caption
 */
class MenuPosition extends Entity {

    // Database Mapping
    protected static $table = "menu_positions";

    public static function fields() {
        return [
            "MenuPositionId" => ['type' => 'integer', 'primary' => true],
            "Caption" => ['type' => 'string', 'required' => true]
        ];
    }


}