<?php

namespace Gdev\MenuManagement\Models;

use Data\Models\Language;
use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;


/**
 * Class Menu
 * @package Gdev\MenuManagement\Models
 *
 * @property integer MenuId
 * @property integer LanguageId
 * @property string Alias
 * @property integer MenuPositionId
 * @property MenuPosition MenuPosition
 * @property Language Language
 */
class Menu extends Entity {

    // Database Mapping
    protected static $table = "menus";

    public static function fields() {
        return [
            "MenuId" => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            "Alias" => ['type' => 'string'],
            "MenuPositionId" => ['type' => 'integer', 'required' => true],
            "LanguageId" => ['type' => 'integer', 'required' => true],
        ];
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity) {
        return [
            'Language' => $mapper->belongsTo($entity, 'Data\Models\Language', 'LanguageId'),
            'MenuPosition' => $mapper->belongsTo($entity, 'Gdev\MenuManagement\Models\MenuPosition', 'MenuPositionId')
        ];
    }

}