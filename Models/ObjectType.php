<?php

namespace Gdev\MenuManagement\Models;

use Spot\Entity;

/**
 * Class ObjectType
 * @package Gdev\MenuManagement\Models
 * 
 * @property integer ObjectTypeId
 * @property string Caption
 */
class ObjectType extends Entity {

    // Database Mapping
    protected static $table = "object_types";

    public static function fields() {
        return [
            "ObjectTypeId" => ['type' => 'integer', 'primary' => true],
            "Caption" => ['type' => 'string', 'required' => true]
        ];
    }


}