<?php

namespace Gdev\MenuManagement\ApiControllers;


use Gdev\MenuManagement\DataManagers\MenuItemsDataManager;

class MenuItemsApiController {
    public static function SaveMenuItem($menuItem) {
        return MenuItemsDataManager::SaveMenuItem($menuItem);
    }

    public static function GetMenuItemsChild($menuItemId) {
        return MenuItemsDataManager::GetMenuItemsChild($menuItemId);
    }

    /**
     * @param $menuId
     * @return \Gdev\MenuManagement\Models\MenuItem[]
     */
    public static function GetMenuItems($menuId) {
        return MenuItemsDataManager::GetMenuItems($menuId);
    }

    public static function GetMenuItem($menuItemId) {
        return MenuItemsDataManager::GetMenuItem($menuItemId);
    }

    public static function DeleteMenuItem($menuItemId) {
        return MenuItemsDataManager::DeleteMenuItem($menuItemId);
    }

    public static function UpdateWeight($weights) {
        return MenuItemsDataManager::UpdateWeight($weights);

    }
}