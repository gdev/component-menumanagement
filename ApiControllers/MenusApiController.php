<?php


namespace Gdev\MenuManagement\ApiControllers;


use Gdev\MenuManagement\DataManagers\MenusDataManager;
use Gdev\MenuManagement\Models\MenuItem;

class MenusApiController {
    public static function SaveMenu($menu) {
        return MenusDataManager::SaveMenu($menu);
    }

    public static function GetMenu($menuId) {
        return MenusDataManager::GetMenu($menuId);
    }

    public static function DeleteMenu($menuId) {
        return MenusDataManager::DeleteMenu($menuId);
    }

    public static function GetMenus() {
        return MenusDataManager::GetMenus();
    }

    public static function GetMenuByPosition($menuPosition, $language) {
        return MenusDataManager::GetMenuByPosition($menuPosition, $language);
    }

    public static function GetTree($menuId) {
        $items = MenuItemsApiController::GetMenuItems($menuId);

        return static::_extractChildren($items, null);
    }

    /**
     * @param MenuItem[] $items
     * @param $parentId
     * @return MenuItem[]
     */
    private static function _extractChildren(&$items, $parentId) {
        $tree = [];

        foreach ($items as $key => $item) {
            if ($item->ParentId == $parentId) {
                unset($items[$key]);
                $item->Children = static::_extractChildren($items, $item->MenuItemId);
                $tree[] = $item;
            }
        }

        return $tree;
    }


}