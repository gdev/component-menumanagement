<?php
namespace Gdev\MenuManagement\DataManagers;


use Gdev\MenuManagement\Repositories\MenusRepository;

class MenusDataManager {
    public static function SaveMenu($menu) {
        return MenusRepository::getInstance()->save($menu);
    }

    public static function GetMenu($menuId) {
        return MenusRepository::getInstance()->get($menuId);
    }

    public static function DeleteMenu($menuId) {
        return MenusRepository::getInstance()->delete(["MenuId" => $menuId]);
    }

    public static function GetMenus() {
        return MenusRepository::getInstance()->all();
    }

    /**
     * @param $menuPosition
     * @param $language
     * @return \Gdev\MenuManagement\Models\Menu[]
     */
    public static function GetMenuByPosition($menuPosition, $language) {
        return MenusRepository::getInstance()->all()->where(["MenuPositionId" => $menuPosition, "LanguageId" => $language]);
    }

}