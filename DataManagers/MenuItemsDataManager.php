<?php

namespace Gdev\MenuManagement\DataManagers;


use Gdev\MenuManagement\Repositories\MenuItemsRepository;

class MenuItemsDataManager {
    public static function SaveMenuItem($menuItem) {
        return MenuItemsRepository::getInstance()->save($menuItem);
    }

    /**
     * @param $menuId
     * @return \Gdev\MenuManagement\Models\MenuItem[]
     */
    public static function GetMenuItems($menuId) {
        return MenuItemsRepository::getInstance()->all()->where(["MenuId" => $menuId])->order(["Weight" => "ASC"]);
    }

    public static function GetMenuItemsChild($menuItemId) {
        return MenuItemsRepository::getInstance()->all()->where(["ParentId" => $menuItemId]);
    }

    public static function DeleteMenuItem($menuItemId) {
        return MenuItemsRepository::getInstance()->delete(["MenuItemId" => $menuItemId]);
    }

    public static function GetMenuItem($menuItemId) {
        return MenuItemsRepository::getInstance()->get($menuItemId);
    }

    public static function UpdateWeight($weights)
    {

        if (count($weights) > 0) {
            foreach ($weights as $weightItem) {
                $item = MenuItemsRepository::getInstance()->get($weightItem['ItemId']);
                $item->Weight = $weightItem['Weight'];
                MenuItemsRepository::getInstance()->save($item);
            }
        }

        return true;
    }
}